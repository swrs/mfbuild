# mfbuild

A set of bash and fontforge scripts to facilitate the work with METAFONT. The scripts help to view a font's characters, generate outline files and convert to .ttf, .woff, etc.

Currently *mfbuild* has these scripts :
- `mfbuild/view` preview a metafont using XDVI
- `mfbuild/trace` trace a metafont to a outline file
- `mfbuild/prepress` prepare outline file
- `mfbuild/generate` create different outline formats like .ttf, .woff, …

*mfbuild* needs the following requirements to be installed :
- [`fontforge`](https://github.com/fontforge/fontforge) Open Source font editor with scripting functionalities
- [`mftrace`](http://lilypond.org/mftrace/) METAFONT tracer

## Installation and Usage

To install *mfbuild* copy the whole directory into the directory of your metafont file(s) and execute all scripts from this directory.

```shell
$ cd path/to/my/metafont
$ mfbuild/view myfont.mf
$ mfbuild/trace myfont.mf MyFont Regular
$ mfbuild/prepress MyFont-Regular.pfa
$ mfbuild/generate MyFont-Regular.pfa
```

## Scripts

### `mfbuild/view [file]`

`mfbuild/view` compiles a metafont file to gf, converts gf-to-dvi and previews the dvi in the XDVI viewer.

```shell
$ mfbuild/view myfont.mf
```

### `mfbuild/trace [file] [fontfamily] [weight] ([directory] [encoding_file])`

`mfbuild/trace` compiles a metafont file and traces it using [`mftrace`](http://lilypond.org/mftrace/) and the `dvips_extended.enc` encoding. Set the `fontfamily` name, `weight` descriptor and `directory` directory with command line arguments.

By default `mftrace` will use the encoding file `dvips_extended.enc` which ships with `mfbuild`. To use an alternative file for encoding set the _path/to/your_encoding.enc_ with the `encoding_file` argument.

The traced outline (PostScript) file will be saved as `[fontfamily]-[weight].pfa` to the same directory as `[file]` or to `[directory]`.

```shell
$ mfbuild/trace myfont.mf MyFont Regular
# => saves to MyFont-Regular.pfa

$ mfbuild/trace myfont.mf OtherFont Italic output
# => saves to output/OtherFont-Italic.pfa

$ mfbuild/trace myfont.mf ThirdFont Bold output 8r.enc
# => traces and encodes using encoding file mfbuild/8r.enc
#    saves to output/ThirdFont-Bold.pfa
```

### `mfbuild/prepress [outline file]`

`mfbuild/prepress` reencodes an outline file to *Unicode*, scales all glyphs to 1024 em and builds accented glyphs. Overrides `[file]`.

You can set a copyright by placing a `copyright.txt` in the `mfbuild/` directory.

```shell
$ mfbuild/prepress MyFont-Regular.pfa
# => saves to MyFont-Regular.pfa
#    sets a copyright if mfbuild/copyright.txt exists
```

### `mfbuild/generate [outline file]`

`mfbuild/generate` generates .ttf, .woff and .woff2 from `[file]`.

```shell
$ mfbuild/generate MyFont-Regular.pfa
# => saves to MyFont-Regular.ttf, MyFont-Regular.woff and MyFont-Regular.woff2
```

## Version

1.0.1

## To Do

- set flexible encoding `.enc` file for `mfbuild/trace`
- built of accented glyphs doesn't work in `mfbuild/prepress`
- Write docs on rendering and converting Metafont files
