## 1.0.1
- added optional copyright text to `mfbuild/prepress`
- added encoding file argument for `mfbuild/trace`

## 1.0.0
- Created mfbuild
